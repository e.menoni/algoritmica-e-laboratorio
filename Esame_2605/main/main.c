/**
 * @file main.c
 * @author Elia Menoni (e.menoni@studenti.unipi.it)
 * @date 2020-05-26
*/

#include <stdio.h>
#include <stdlib.h>

//Nodo dell'albero
struct nodo {
  unsigned int chiave;
  struct nodo *nodo_DX;
  struct nodo *nodo_SX;
  _Bool colore; /* 0 Bianco / 1 Rosso */
};
typedef struct nodo nodo;

//Creazione nuovo nodo
nodo *nuovo_nodo(unsigned int chiave, unsigned int colore) {
  nodo *temp = (nodo *)malloc(sizeof(nodo));
  if(temp == NULL) exit(0);
  temp->chiave = chiave;
  temp->colore = (_Bool)colore;
  temp->nodo_DX = NULL;
  temp->nodo_SX = NULL;
  return temp;
}

//Funzione di inserimento di un nodo nell'albero
void insert(nodo **albero, unsigned int chiave, unsigned int colore) {
  //Se l'abero è vuoto
  if (*albero == NULL) {
    *albero = nuovo_nodo(chiave, colore);
  }
  //Se il nodo corrente è maggiore di quello da inserire
  else if (chiave <= (*albero)->chiave) {
    if ((*albero)->nodo_SX == NULL) {
      (*albero)->nodo_SX = nuovo_nodo(chiave, colore);
    } else
      //Mi sposto sul sotto albero sinistro
      insert(&((*albero)->nodo_SX), chiave, colore);
  }
  //Se il nodo corrente è inferiore di quello da inserire
  else if ((*albero)->nodo_DX == NULL) {
    (*albero)->nodo_DX = nuovo_nodo(chiave, colore);
  } else
    //Mi sposto sul sottoalbero destro
    insert(&((*albero)->nodo_DX), chiave, colore);
  return;
}

//Funzione per la lettura e l'inserimento dei dati nell'albero binario
nodo *popolazione_albero(int n_input) {
  unsigned int chiave, colore;
  nodo *albero = NULL;
  //Per ogni valore
  for (int i = 0; i < n_input; i++) {
    //Leggo i dati
    scanf("%d\n%d", &chiave, &colore);
    //Inserisco nell'albero
    insert(&albero, chiave, colore);
  }
  //Restituisco il puntatore alla radice
  return albero;
}

//Verifica condizione come richiesto
int verifica_condizione(nodo *albero) {
  //Se il nodo corrente non esiste (FOGLIA)
  if (albero == NULL)
    return 0;
  //Verifico condizione sul sottoalbero sinistro
  int SX = verifica_condizione(albero->nodo_SX);
  //Se il sottoalbero non rispetta la condizione restituisco -1
  if (SX == -1)
    return -1;
  //Verifico condizione sul sottoalbero destro
  int DX = verifica_condizione(albero->nodo_DX);
  //Se il sottoalbero non rispetta la condizione restituisco -1
  if (DX == -1)
    return -1;
  //Verifico condizione sul nodo corrente
  if (SX - 1 == DX || DX - 1 == SX || SX == DX)
    //Se la condizione è verificata restituisco l'h max
    if (SX >= DX)
      return SX + (albero->colore == 0 ? 1 : 0);
    else
      return DX + (albero->colore == 0 ? 1 : 0);
  //Altrimenti restituisco -1
  else
    return -1;
}

//Main
int main(void) {
  int n_input;
  scanf("%d", &n_input);
  //Popolo l'albero
  nodo *albero = popolazione_albero(n_input);
  //Verifico condizione sull'albero
  if (verifica_condizione(albero) != -1)
    printf("TRUE\n");
  else
    printf("FALSE\n");
  return 0;
}
