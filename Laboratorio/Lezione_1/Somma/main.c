/**
 * @author Elia Menoni - 598375 (e.menoni@studenti.unipi.it)
 * @brief 
 * 
 * @copyright Copyright (c) 2019
 */

//Librerie
#include <stdio.h>

//Macro

//Strutture dati

//Funzioni

//Main
int main(void) {
  unsigned int sum = 0, temp;
  while (scanf("%u", &temp) == 1 && temp != 0)
    sum += temp;
  printf("%u", sum);
  return 0;
}