/**
 * @author Elia Menoni - 598375 (e.menoni@studenti.unipi.it)
 * @brief 
 * 
 * @copyright Copyright (c) 2019
 */

//Librerie
#include <stdio.h>
#include <math.h>

//Macro

//Strutture dati

//Funzioni

//Main
int main(void) {
  unsigned int num;
  scanf("%d", &num);

  if (num % 2 == 0) {
    printf("0");
    return 0;
  }

  for (unsigned int i = sqrt(num); i < num; i++) {
    if (num % i == 0) {
      printf("0");
      return 0;
    }
    if (i + 1 % 2 == 0)
      i++;
  }
  printf("1");
  return 0;
}