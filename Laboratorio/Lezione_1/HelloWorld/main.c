/**
 * @author Elia Menoni - 598375 (e.menoni@studenti.unipi.it)
 * @brief 
 * 
 * @copyright Copyright (c) 2019
 */

//Librerie
#include <stdio.h>

//Macro

//Strutture dati

//Funzioni

//Main
int main(void) {
  printf("Ciao Mondo!\n");
  return 0;
}