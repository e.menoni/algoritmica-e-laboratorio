\documentclass{report}
%---------------------------------------------
\usepackage{blindtext}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\usepackage{pdfpages}
\usepackage{listings}
\lstset{
  language=C,
  frame=tb,
  escapeinside={@:}{:@},
  showstringspaces=false,
  basicstyle=\ttfamily
}
\usepackage{xcolor,colortbl}
\definecolor{brightgreen}{rgb}{0.4, 1.0, 0.0}
\definecolor{ferrarired}{rgb}{1.0, 0.11, 0.0}
\definecolor{amber}{rgb}{1.0, 0.75, 0.0}
\usepackage{fancyvrb}
\usepackage[margin=3cm]{geometry}
\usepackage{tikz}
%---------------------------------------------
\title{Algoritmica}
\author{Elia Menoni}
%---------------------------------------------
\begin{document}
\pagestyle{plain}
%---------------------------------------------
\maketitle
\tableofcontents
\newpage
%---------------------------------------------
%---Capitoli
\chapter{Introduzione}
%---------------------------------------------
\section{Definizione di algoritmo}
Un algoritmo è una sequenza finita di passi (operazioni elementari) univocamente definiti
che, se eseguiti, portano alla soluzione di un problema.\\
Definizione di D.E.Knuth
\subsection{Correttezza}
Un algoritmo corretto risolve sempre e solo il problema per cui è stato progettato.
\subsection{Efficienza}
Un algoritmo efficiente risolve il problema nel minor tempo possibile e con il minimo consumo di risorse.
%---------------------------------------------
\chapter{Complessità computazionale}
%---------------------------------------------
Ogni algoritmo ha un costo per ogni specifica istanza che possiamo classificare in termini di:
\begin{itemize}
	\item Tempo $\to$ operazioni elementari
	\item Spazio $\to$ celle di memoria utilizzate durante l'esecuzione
\end{itemize}
\textbf{I costi di un algoritmo vengono calcolati in funzione dell'input espressi come ordine di grandezza}
\begin{align*}
	\text{per esempio: }n^2
\end{align*}
\textbf{Un algoritmo asintoticamente più efficiente di un altro sarà il migliore indifferentemente dalle dimenioni dell'input e indipendentemente dalla tecnologia}
%---------------------------------------------
\section{Caso pessimo, ottimo, medio}
Sia $A$ un algoritmo e $I$ un istanza di quest'ultimo identifichiamo con $|I| = n$ la dimensione dell'input mentre $T_A(I)$ il numero di passi dell'algoritmo $A$ per risolvere l'istanza $I$
\begin{itemize}
	\item \textbf{Complessità al caso pessimo}\\$max_{I,|I|=n}\{T_A(I)\}$ sarà il costo massimo su tutte le possibili istanze di dimensione $n$
	\item \textbf{Complessità al caso ottimo}\\$min_{I,|I|=n}\{T_A(I)\}$ sarà il costo minimo su tutte le possibili istanze di dimensione $n$
	\item \textbf{Complessità al caso medio}\\$T_A(I)$ sarà il costo medio su tutte le possibili istanze di dimensione $n$
\end{itemize}
%---------------------------------------------
\chapter{Limiti del calcolo}
\section{Notazione asintotica}
Gli ordini di grandezza sono insiemi infiniti i quali includono tutte le funzioni che, rispetto ad una data funzione $g(n)$, hanno un certo comportamento asintotico.
\begin{align*}
	f(n) &  & \text{La nostra funzione}      \\
	g(n) &  & \text{Funzione di riferimento}
\end{align*}
\newpage
\subsection{Notazione $\Theta$}
La notazione $\Theta$ o limite asintotico stretto è così definita:
\begin{align*}
	\Theta(g(n)) = \{f(n)|\exists_{C_1,C_2,n_0>0}:\forall_{n\geq n_0}\ 0\leq C_1g(n)\leq f(n)\leq C_2g(n)\}
\end{align*}
Indichiamo "$f(n)$ è in $\Theta(g(n))$" nel seguente modo:
\begin{align*}
	f(n)\in\Theta(g(n)) &  & \text{oppure} &  & f(n)=\Theta(g(n))
\end{align*}
\begin{center}
	\tikzset{every picture/.style={line width=0.75pt}} %set default line width to 0.75pt        
	\begin{tikzpicture}[x=50pt,y=30pt,yscale=1,xscale=1]
		%uncomment if require: \path (0,392); %set diagram left start at 0, and has height of 392
		\draw[->] (-3,0) -- (4.2,0) node[right] {$x$};
		\draw[->] (0,-3) -- (0,4.2) node[above] {$y$};
		\draw[scale=0.2,domain=-1:3,smooth,variable=\y,red]  plot ({\y-1},{exp(\y)});
		\draw[scale=0.2,domain=-1:3,smooth,variable=\y,green]  plot ({\y+1},{exp(\y)});
		\draw[scale=0.2,domain=-1:5,smooth,variable=\x,blue] plot ({\x},{exp(\x)/10});
		\node at (1.2,1) {\footnotesize $C_1g(n)$};
		\node at (1.2,3.75) {\footnotesize $f(n)$};
		\node at (0.40,4.5) {\footnotesize $C_2g(n)$};
	\end{tikzpicture}
\end{center}
\textbf{Esempio}
\begin{align*}
	\sum_{i=0}^d a_{i}n^{i}= & a_{d}n^{d}+a_{d-1}n^{d-1}+...+a_{1}n+a_{0} \\
	=                        & \Theta(n^d)
\end{align*}
$f(n)$ è un polinomio $\to$ il termine di grado massimo con costante moltiplicativa $\neq0$
\newpage
\subsection{Notazione $O$}
La notazione $O$ (O grande) o limite asintotico superiore è così definita:
\begin{align*}
	O(g(n)) = \{f(n)|\exists_{C,n_0>0}:\forall_{n\geq n_0}\ 0\leq f(n)\leq Cg(n)\}
\end{align*}
Indichiamo "$f(n)$ è in $O(g(n))$" nel seguente modo:
\begin{align*}
	f(n)\in O(g(n)) &  & \text{oppure} &  & f(n)= O(g(n))
\end{align*}
\begin{center}
	\tikzset{every picture/.style={line width=0.75pt}} %set default line width to 0.75pt        
	\begin{tikzpicture}[x=50pt,y=30pt,yscale=1,xscale=1]
		%uncomment if require: \path (0,392); %set diagram left start at 0, and has height of 392
		\draw[->] (-3,0) -- (4.2,0) node[right] {$x$};
		\draw[->] (0,-3) -- (0,4.2) node[above] {$y$};
		\draw[scale=0.2,domain=-1:3,smooth,variable=\y,red]  plot ({\y-1},{exp(\y)});
		\draw[scale=0.2,domain=-1:3,smooth,variable=\y,green]  plot ({\y+1},{exp(\y)});
		\node at (1.2,3.75) {\footnotesize $f(n)$};
		\node at (0.40,4.5) {\footnotesize $Cg(n)$};
	\end{tikzpicture}
\end{center}
\newpage
\subsection{Notazione $\Omega$}
La notazione $\Omega$ o limite asintotico inferiore è così definita:
\begin{align*}
	\Omega(g(n)) = \{f(n)|\exists_{C,n_0>0}:\forall_{n\geq n_0}\ 0\leq Cg(n)\leq f(n)\}
\end{align*}
Indichiamo "$f(n)$ è in $\Omega(g(n))$" nel seguente modo:
\begin{align*}
	f(n)\in \Omega(g(n)) &  & \text{oppure} &  & f(n)= \Omega(g(n))
\end{align*}
\begin{center}
	\tikzset{every picture/.style={line width=0.75pt}} %set default line width to 0.75pt        
	\begin{tikzpicture}[x=50pt,y=30pt,yscale=1,xscale=1]
		%uncomment if require: \path (0,392); %set diagram left start at 0, and has height of 392
		\draw[->] (-3,0) -- (4.2,0) node[right] {$x$};
		\draw[->] (0,-3) -- (0,4.2) node[above] {$y$};
		\draw[scale=0.2,domain=-1:3,smooth,variable=\y,green]  plot ({\y-1},{exp(\y)});
		\draw[scale=0.2,domain=-1:3,smooth,variable=\y,red]  plot ({\y+1},{exp(\y)});
		\node at (0.4,4.5) {\footnotesize $f(n)$};
		\node at (1.2,3.75) {\footnotesize $Cg(n)$};
	\end{tikzpicture}
\end{center}
\newpage
\subsection{Notazione $o$}
La notazione $o$ (o piccolo) o limite asintotico superiore non stretto è così definita:
\begin{align*}
	o(g(n)) = \{f(n)|\exists_{C>0}\ \exists_{n_o>0}:\forall_{n\geq n_0}\ 0\leq f(n) < Cg(n)\}
\end{align*}
Indichiamo "$f(n)$ è in $o(g(n))$" nel seguente modo:
\begin{align*}
	f(n)\in o(g(n)) &  & \text{oppure} &  & f(n)= o(g(n))
\end{align*}
\begin{center}
	\tikzset{every picture/.style={line width=0.75pt}} %set default line width to 0.75pt        
	\begin{tikzpicture}[x=50pt,y=30pt,yscale=1,xscale=1]
		%uncomment if require: \path (0,392); %set diagram left start at 0, and has height of 392
		\draw[->] (-3,0) -- (4.2,0) node[right] {$x$};
		\draw[->] (0,-3) -- (0,4.2) node[above] {$y$};
		\draw[scale=0.2,domain=-1:3,smooth,variable=\y,red]  plot ({\y-1},{exp(\y)});
		\draw[scale=0.2,domain=-1:3,smooth,variable=\y,green]  plot ({\y+1},{exp(\y)});
		\node at (1.2,3.75) {\footnotesize $f(n)$};
		\node at (0.40,4.5) {\footnotesize $Cg(n)$};
	\end{tikzpicture}
\end{center}
\newpage
\subsection{Notazione $\omega$}
La notazione $\omega$ o limite asintotico inferiore non stretto è così definita:
\begin{align*}
	\omega(g(n)) = \{f(n)|\exists_{C>0}\ \exists_{n_o>0}:\forall_{n\geq n_0}\ 0\leq Cg(n) < f(n)\}
\end{align*}
Indichiamo "$f(n)$ è in $\omega(g(n))$" nel seguente modo:
\begin{align*}
	f(n)\in \omega(g(n)) &  & \text{oppure} &  & f(n)= \omega(g(n))
\end{align*}
\begin{center}
	\tikzset{every picture/.style={line width=0.75pt}} %set default line width to 0.75pt        
	\begin{tikzpicture}[x=50pt,y=30pt,yscale=1,xscale=1]
		%uncomment if require: \path (0,392); %set diagram left start at 0, and has height of 392
		\draw[->] (-3,0) -- (4.2,0) node[right] {$x$};
		\draw[->] (0,-3) -- (0,4.2) node[above] {$y$};
		\draw[scale=0.2,domain=-1:3,smooth,variable=\y,green]  plot ({\y-1},{exp(\y)});
		\draw[scale=0.2,domain=-1:3,smooth,variable=\y,red]  plot ({\y+1},{exp(\y)});
		\node at (0.4,4.5) {\footnotesize $f(n)$};
		\node at (1.2,3.75) {\footnotesize $Cg(n)$};
	\end{tikzpicture}
\end{center}
\newpage
\section{Limiti alla complessità}
Dato un algoritmo $A$ che risolve un problema $\Pi$ posso determinare un limite superiore alla complessità di $\Pi$ che indico con $T_A(n)$\\
Se per $\Pi$ posso determinare anche un limite inferiore alla sua complessità che indico con $L(n)$ allora ho i margini per migliorare il mio algoritmo $A$
\begin{itemize}
  \item Se $T_a(n)=L(n) \implies A$ è un algoritmo ottimo
  \item Se $T_a(n)\neq L(n) \implies$ posso migliorare $A$
  \item Se $T_a(n)\neq L(n) \implies$ posso migliorare il limite inferiore
\end{itemize}
\subsection{Limite inferiore basato sulla dimensione dell'input}
Se la soluzione di $\Pi$ irhiceda l'esame di tutti i dati in input, allora la dimensione dell'input
è un limite inferiore alla complessita di $\Pi$ $\implies \Omega(n)$
\subsection{Limite inferiore per eventi contabili}
Se la ripetizione di un certo evento è indispensabile per risolvere $\Pi$, allora il numero di volte
che si deve ripetere l'evento (moltiplicato per il suo costo) è un limite inferiore alla complessita di $\Pi$\\~\\
\textbf{Esempio: } $\Pi$: generare tutte le permutazioni di $n$ elementi. Evento: formazione di una permutazione $\implies L_\Pi(n) = \Omega(n!)$
\subsection{Limite inferiore con albero di decisione}
Questo procedimento si applica a problemi risolvibili attraverso una sequenza di decisioni il cui numero da una misura (limite inferiore) alla complessità
dell'algoritmo.
\begin{itemize}
  \item Nodi interni $\rightarrow$ confronti, decisioni
  \item Foglie $\rightarrow$ soluzioni (\#Foglie $\geq$ \#Possibili soluzioni)
  \item Cammino radice-foglia $\rightarrow$ sequenza di confronti ed esito per uno specifico caso (possibile esecuzione dell'algoritmo)
  \item Lunghezza del cammino radice-foglia $\rightarrow$ durata di quella esecuzione
  \item Altezza massima dell'AdD $\rightarrow$ complessità al caso pessimo
\end{itemize}
\textbf{NB}: un limite inferiore per l'altezza di un AdD per un problema $\Pi$, equivale ad un limite inferiore al numero di passi necessari per risolvere $\Pi$\\~\\
Poichè l'AdD è tipicamente binario abbiamo che:
\begin{itemize}
  \item Altezza minima $\geq$ $\log_2$\#Foglie $\rightarrow$ Limite inferiore alla complessità
  \item \#Foglie $\geq$ \#Soluzioni possibili
  \item $L(\Pi)=L(n)=\Omega(\log_2n)$ che per la notazione asintotica equivale a $\Omega(\log n)$
\end{itemize}
\newpage
\section{Complessità espressa con relazioni di ricorrenza}
In generale vogliamo studiare il comportamento asintotico di una funzione $T(n)$ così definità:
\[
  T(n)=
  \begin{cases}
    \Theta(1) & n\leq h\\
    \underbrace{\alpha_1T(n_1)+ ... + \alpha_kT(n_k)}_\text{Chiamata ricorsiva}+\underbrace{f(n)}_\text{Costo dividi + combine}&n>h
  \end{cases}
\]
\subsection{Tipi di relazioni di ricorrenza}
Esistono due tipi di relazioni di ricorrenza:\\~\\
\textbf{Relazioni bilanciate}
\[
  T(n)=
  \begin{cases}
    \Theta(1) & n\leq h\\
    aT(\frac{n}{b})+f(n)&n>h
  \end{cases}
\]
\textbf{Relazioni di ordine K}
\[
  T(n)=
  \begin{cases}
    \Theta(1) & n\leq h\\
    \alpha_1T(n-1)+ ... + \alpha_kT(n-k)&n>h
  \end{cases}
\]
\subsection{Soluzione della ricorrenza}
Esistono vari modi per risolvere una ricorrenza ed arrivare ad esprimerla in una forma chiusa:
\begin{itemize}
  \item \textbf{Metodo iterativo} si sviluppa la ricorrenza fino al caso base
  \item \textbf{Metodo di sostituzione} si ipotizza una soluzione e la si dimostra per induzione
  \item \textbf{Albero di ricorsione} ausilio grafico che rappresenta i costi ad ogni livello
  \item \textbf{Teorama dell'esperto} il teorema più usato per risolvere le relazioni bilanciate 
\end{itemize}
\newpage
\section{Teorema dell'esperto}
Siano $a\geq1$, $b>1$, $n_0\geq0$ costanti, $f(n)$ funzione e $T(n)$ così definita:
\[
  T(n)=
  \begin{cases}
    \Theta(1) & n\leq n_0\\
    aT(\frac{n}{b})+f(n)&n>n_0
  \end{cases}
\]
Allora $T(n)$ è limitato asintoticamente come segue:
\begin{itemize}
  \item Se $f(n)\in O(n^{\log_ba-\epsilon})$ per $\epsilon > 0$ $\implies$ $T(n)\in\Theta(n^{\log_ba})$
  \item Se $f(n)\in \Theta(n^{\log_ba})$ $\implies$ $T(n)\in\Theta(n^{\log_ba}*\log n)$
  \item Se $f(n)\in \Omega(n^{\log_ba+\epsilon})$ per $\epsilon > 0$ $\land af(\frac{n}{b})\leq cf(n)$ per $c < 1$ e $\forall n$ sufficientemente grande $\implies$ $T(n)\in\Theta(f(n))$
\end{itemize}
\subsection{Casi non applicabili}
\subsubsection{Gap tra i casi 1 e 2}
$f(n)$ minore di $n^{\log_ba}$, ma non di un fattore polinomiale.\\~\\
\textbf{Es}:
\begin{align*}
  f(n)\in\Theta\Bigl(\frac{n^{\log_ba}}{\log n}\Bigr)
\end{align*}
\subsubsection{Gap tra i casi 2 e 3}
$f(n)$ maggiore di $n^{\log_ba}$, ma non di un fattore polinomiale.\\~\\
\textbf{Es}:
\begin{align*}
  f(n)\in\Theta\Bigl(n^{\log_ba}*\log n\Bigr)
\end{align*}
\newpage
\subsection{Proprietà delle notazioni asintotiche}
\begin{itemize}
	\item $f(n) \in \Theta(g(n))\implies f(n)\in\Omega(g(n))$
	\item $f(n) \in \Theta(g(n))\implies f(n)\in O(g(n))$
	\item $f(n) \in \Theta(g(n))\implies f(n)\in\Omega(g(n)) \wedge f(n)\in O(g(n))$
	\item $f(n) \in O(g(n))\implies g(n)\in O(g(n))$
	\item $f(n) \in \begin{vmatrix}O\\\Theta\\\Omega\end{vmatrix}(g_1(n)) \wedge g_1(n)\in\begin{vmatrix}O\\\Theta\\\Omega\end{vmatrix}(g_2(n)) \implies f(n)\in\begin{vmatrix}O\\\Theta\\\Omega\end{vmatrix}(g_2(n))$
	\item $f_1(n) \in \begin{vmatrix}O\\\Theta\\\Omega\end{vmatrix}(g_1(n)) \wedge f_2(n)\in\begin{vmatrix}O\\\Theta\\\Omega\end{vmatrix}(g_2(n)) \implies f_1(n)+f_2(n)\in\begin{vmatrix}O\\\Theta\\\Omega\end{vmatrix}(g_1(n)+g_2(n))\in\begin{vmatrix}O\\\Theta\\\Omega\end{vmatrix}(max(g_1(n),g_2(n)))$
	\item $d>0\text{ costante }f(n)\in\begin{vmatrix}O\\\Theta\\\Omega\end{vmatrix}(g(n))\implies d*f(n)\in\begin{vmatrix}O\\\Theta\\\Omega\end{vmatrix}(g(n))$
	\item $f(n)\in\Theta(g(n))\iff g(n)\in\Theta(f(n))$
	\item $f(n)\in\Theta(f(n))$
	\item $f(n)\in O(f(n))$
	\item $f(n)\in\Omega(f(n))$
	\item $f_1(n)\in O(g_1(n))\wedge f_2(n)\in O(g_2(n))\implies f_1(n)*f_2(n)\in O(g_1(n)*g_2(n))$
	\item $O(\log(n))\subseteq O(n^h)\subseteq O(n^k)\subseteq O(n^k\log(n))\subseteq O(n^{k+1})\subseteq O(a^n)\subseteq O(b^n)\ \ \ \forall_{0<h\leq k}\ \ \ \forall_{1<a<b}$
	\item $f(n)\in o(g(n))\iff \lim_{n\to\infty}\frac{f(n)}{g(n)} = 0$
	\item $\log^k(n)\in o(n^\epsilon)\ \ \ \forall_{k>0}\ \ \ \forall_{\epsilon>0}$
	\item $n^b\in o(a^n)\ \ \ \forall_{b>0, a>1}$
	\item $f(n)\in\omega(g(n))\iff \lim_{n\to\infty}\frac{f(n)}{g(n)}=+\infty$
	\item $f(n)\in o(g(n))\iff g(n)\in \omega(f(n))$
	\item $\sum\limits_{i=1}^{n}i = \frac{n(n-1)}{2} = \Theta(n^2)$
	\item $\sum\limits_{i=0}^{n}x^i = 1+x+x^2+x^3+...+x^n=\frac{x^{n-1}+1}{n+1}\ \ \ \forall_{m>1}$
	\item $\sum\limits_{i=0}^{\infty}x^i=\frac{1}{1-x}\ \ \ \forall_{|x|<1}$
	\item $\log_c(a*b)=\log_ca+\log_cb$
	\item $\log_ba^n=n\log_ca$
	\item $\log_ba=\frac{\log_ca}{\log_cb}$
\end{itemize}

%---------------------------------------------
\chapter{Algoritmi fondamentali}
\section{Ricerca sequenziale}
\subsection{Problema}
\textbf{Input:} Array $A$ di $n$ numeri interi, una chiave $K$ intera.\\
\textbf{Output:} $i:A[i]=k$ se esiste altrimenti $-1$
\subsection{Codice}
\begin{lstlisting}[frame=single]
  ricerca(A,k){
    pos=-1;
    i=1;
    while(i<=n && pos=-1) do{
      if(A[i]=k)then
        pos=i;
      else
      i++;
    }
    return pos;
  }
\end{lstlisting}
\newpage
\subsection{Esempio}
\textbf{Input:} Array $A$ = \begin{tabular}{|c|c|c|c|c|c|}
	\hline
	5 & 2 & 4 & 6 & 1 & 3 \\
	\hline
\end{tabular}\ , $K=6$\\
\textbf{\\Ordinamento in azione:}\\~\\
$A=$\begin{tabular}{|c|c|c|c|c|c|}
	\hline
	\cellcolor{ferrarired}5 & 2 & 4 & \cellcolor{brightgreen}6 & 1 & 3 \\
	\hline
\end{tabular}\hfill
Leggenda \begin{tabular}{|c|c|}
	\hline
	\cellcolor{brightgreen}Da trovare & \cellcolor{ferrarired}Corrente \\
	\hline
\end{tabular}\\
$A=$\begin{tabular}{|c|c|c|c|c|c|}
	\hline
	5 & \cellcolor{ferrarired}2 & 4 & \cellcolor{brightgreen}6 & 1 & 3 \\
	\hline
\end{tabular}\hfill $i = 2$\\
$A=$\begin{tabular}{|c|c|c|c|c|c|}
	\hline
	5 & 2 & \cellcolor{ferrarired}4 & \cellcolor{brightgreen}6 & 1 & 3 \\
	\hline
\end{tabular}\hfill $i = 3$\\
$A=$\begin{tabular}{|c|c|c|c|c|c|}
	\hline
	5 & 2 & 4 & \cellcolor{ferrarired}6 & 1 & 3 \\
	\hline
\end{tabular}\hfill $i = 4$
\subsection{Come funziona}
Il codice scorre linearmente l'array dall'inizio alla fine e termina solo quando trova l'elemento $K$ o quando arriva alla fine dell'array.
\subsection{Complessita di Selection-Sort}
\begin{lstlisting}[frame=single]
  ricerca(A,k){
    pos=-1; @:\hfill 1 volta * $\Theta(1)$:@
    i=1; @:\hfill 1 volta * $\Theta(1)$:@
    while(i<=n && pos=-1) do{ @:\hfill tw volte * $\Theta(1)$:@
      if(A[i]=k)then @:\hfill tw-1 volte * $\Theta(1)$:@
        pos=i;
      else
      i++; 
    }
    return pos; @:\hfill 1 * $\Theta(1)$:@
  }
\end{lstlisting}
%\textbf{NB:} $t_j$ è il numero di volte che si valuta la guardia del \verb-while- nella $j_{esima}$ iterazione del \verb-for-
\subsubsection{Calcolo della complessità}
\begin{align*}
	T(n) = & \Theta(1)+tw*\Theta(1)+(tw-1)\Theta(1)
\end{align*}
\textbf{Calcolo al caso ottimo:}
\begin{align*}
	tw=2\text{ lo trovo subito}
\end{align*}
\begin{align*}
	T(n) = & \Theta(1)+2\Theta(1)+\Theta(1) \\
	=      & 4*\Theta(1)                    \\
	=      & \Theta(1)
\end{align*}\\~\\~\\~\\~\\~\\~\\
\textbf{Calcolo al caso pessimo:}
\begin{align*}
	tw=n+1\text{ non lo trovo}
\end{align*}
\begin{align*}
	T(n) = & \Theta(1)+(n+1)\Theta(1)+\Theta(1)+n\Theta(1) \\
	=      & \Theta(n)
\end{align*}\\~\\
\textbf{Calcolo al caso medio:}
\begin{align*}
	\text{Confronti: }1,2,3,4,...,n,n
\end{align*}
\begin{align*}
	C(n)= & \frac{1+2+3+4+...+n+n}{n+1}                   \\
	=     & \frac{1}{n+1}\biggl(\frac{n(n+1)}{2}+n\biggr) \\
	=     & \frac{n}{2}+\frac{n}{n+1}                     \\
	<     & \frac{n}{2}+1                                 \\
	=     & \Theta(n)
\end{align*}
\newpage
\section{Divide et impera}
\subsection{Problema}
\textbf{Input:} Array $A$ ordinato di $n$ numeri interi, una chiave $K$ intera.\\
\textbf{Output:} $i:A[i]=k$ se esiste altrimenti $-1$
\subsection{Codice}
\begin{lstlisting}[frame=single]
  ricerca-binaria(A,p,r,k){
    if(p>r)then return -1; 
    q=(p+r)/2;
    if(A[q]=K)then return q;
    if(A[q]<k)then
      return ricerca-binaria(A,q+1,r,K);
    else
      return ricerca-binaria(A,p,q-1,K); 
  }
\end{lstlisting}
\subsection{Esempio}
\textbf{Input:} Array $A$ = \begin{tabular}{|c|c|c|c|c|c|}
	\hline
	1 & 2 & 3 & 4 & 5 & 6 \\
	\hline
\end{tabular}\ , $K=4$\\
\textbf{\\Ordinamento in azione:}\\~\\
$A=$\begin{tabular}{|c|c|c|c|c|c|}
	\hline
	1 & 2 & \cellcolor{ferrarired}3 & \cellcolor{brightgreen}4 & 5 & 6 \\
	\hline
\end{tabular}\hfill
Leggenda \begin{tabular}{|c|c|c|}
	\hline
	\cellcolor{brightgreen}Da trovare & \cellcolor{ferrarired}Corrente & \cellcolor{amber}Scartato \\
	\hline
\end{tabular}\\
$A=$\begin{tabular}{|c|c|c|c|c|c|}
	\hline
	\cellcolor{amber}1 & \cellcolor{amber}2 & \cellcolor{amber}3 & \cellcolor{brightgreen}4 & \cellcolor{ferrarired}5 & 6 \\
	\hline
\end{tabular}\hfill $q = 5$\\
$A=$\begin{tabular}{|c|c|c|c|c|c|}
	\hline
	\cellcolor{amber}1 & \cellcolor{amber}2 & \cellcolor{amber}3 & \cellcolor{brightgreen}4 & \cellcolor{amber}5 & \cellcolor{amber}6 \\
	\hline
\end{tabular}\hfill $q = 4$
\subsection{Come funziona}
Il codice si sposta a metà dell'array ogni volta che la ricorsione comincia. Se il numero corrente è quello cercato viene restituito l'indice dell'array che lo contiene.
Se il numero corrente è $<$ di quello cercato allora la ricorsione ri comincia sul lato destro dell'array se invece è $<$ quest'ultima ri comincia sil lato sinistro dell'array.
\newpage
\subsection{Complessita di Selection-Sort}
\begin{lstlisting}[frame=single]
  ricerca-binaria(A,p,r,k){
    if(p>r)then return -1; @:\hfill $\Theta(1)$:@
    q=(p+r)/2;@:\hfill $\Theta(1)$:@
    if(A[q]=K)then return q;@:\hfill $\Theta(1)$:@
    if(A[q]<k)then
      return ricerca-binaria(A,q+1,r,K);@:\hfill $T(\frac{n}{2}+\Theta(1))$:@
    else
      return ricerca-binaria(A,p,q-1,K);@:\hfill $T(\frac{n}{2}+\Theta(1))$:@
  }
\end{lstlisting}
%\textbf{NB:} $t_j$ è il numero di volte che si valuta la guardia del \verb-while- nella $j_{esima}$ iterazione del \verb-for-
\subsubsection{Calcolo della complessità}
\[
	T(n)=
	\begin{cases}
		\Theta(1)                & n=1     \\
		T(\frac{n}{2})+\Theta(1) & n\geq 2
	\end{cases}
\]
\textbf{Risoluzione della ricorsione:}
\begin{align*}
	T(n) = & T(\frac{n}{2})+c\\
	=&T(\frac{n}{4})+2c\\
	=&...\\
	=&T(\frac{n}{2^i})+ic\\
	=&T(1)+c*\log_2n\\
	=&\Theta(1)+c*\log_2n\in \Theta(\log_2n)
\end{align*}
\newpage
\section{Insertion-Sort}
\subsection{Problema}
\textbf{Input:} Array $A$ di $n$ numeri interi.\\
\textbf{Output:} Array $A$ ordinato: $A[1] \leq A[2] \leq A[3] \leq ... \leq A[n]$
\subsection{Codice}
\begin{lstlisting}[frame=single]
  insertion-sort(A,n){
    for(j = 2) to n do{
      k = A[j];
      i = j - 1;
      while(i > 0 && A[i] > k){
        A[i + 1] = A[i];
        i--;
      }
      A[i + 1] = k;
    }
  }
\end{lstlisting}
In questo esempio un operazione costa 1 (assegnamento/controlli/incrementi/ecc.)
\subsection{Esempio}
\textbf{Input:} Array $A$ = \begin{tabular}{|c|c|c|c|c|c|}
	\hline
	5 & 2 & 4 & 6 & 1 & 3 \\
	\hline
\end{tabular}\\
\textbf{\\Ordinamento in azione:}\\~\\
$A=$\begin{tabular}{|c|c|c|c|c|c|}
	\hline
	\cellcolor{brightgreen}5 & \cellcolor{ferrarired}2 & 4 & 6 & 1 & 3 \\
	\hline
\end{tabular}\hfill
Leggenda \begin{tabular}{|c|c|}
	\hline
	\cellcolor{brightgreen}Ordinato & \cellcolor{ferrarired}Da ordinare \\
	\hline
\end{tabular}\\
$A=$\begin{tabular}{|c|c|c|c|c|c|}
	\hline
	\cellcolor{brightgreen}2 & \cellcolor{brightgreen}5 & \cellcolor{ferrarired}4 & 6 & 1 & 3 \\
	\hline
\end{tabular}\hfill $j = 3$\\
$A=$\begin{tabular}{|c|c|c|c|c|c|}
	\hline
	\cellcolor{brightgreen}2 & \cellcolor{brightgreen}4 & \cellcolor{brightgreen}5 & \cellcolor{ferrarired}6 & 1 & 3 \\
	\hline
\end{tabular}\hfill $j = 4$\\
$A=$\begin{tabular}{|c|c|c|c|c|c|}
	\hline
	\cellcolor{brightgreen}2 & \cellcolor{brightgreen}4 & \cellcolor{brightgreen}5 & \cellcolor{brightgreen}6 & \cellcolor{ferrarired}1 & 3 \\
	\hline
\end{tabular}\hfill $j = 5$\\
$A=$\begin{tabular}{|c|c|c|c|c|c|}
	\hline
	\cellcolor{brightgreen}1 & \cellcolor{brightgreen}2 & \cellcolor{brightgreen}4 & \cellcolor{brightgreen}5 & \cellcolor{brightgreen}6 & \cellcolor{ferrarired}3 \\
	\hline
\end{tabular}\hfill $j = 6$\\
$A=$\begin{tabular}{|c|c|c|c|c|c|}
	\hline
	\cellcolor{brightgreen}1 & \cellcolor{brightgreen}2 & \cellcolor{brightgreen}3 & \cellcolor{brightgreen}4 & \cellcolor{brightgreen}5 & \cellcolor{brightgreen}6 \\
	\hline
\end{tabular}\hfill $j = 7 = n+1$\\
\subsection{Come funziona}
Il sottoarray $A[1,j-1]$ è, di fatto, ordinato per ogni elemento successivo scorro il sottoarray ordinato partendo dalla fine finchè non trovo un elemento più piccolo.
Quando lo trovo sposto tutti gli elementi dell'array succevi nella cella successiva ed inserisco nella cella appena liberata l'elemento da ordinare. Il primo elemento dell'array è banalmente ordinato.
\subsection{Invariante di ciclo}
All'inizio dell'interazione $j_{esima}$ del \verb-for- il sottoarray $A[1,j-1]$ è ordinato e contiene gli elementi che vi erano all'inizio.
\subsection{Dimostrazione per induzione}
\begin{enumerate}
	\item Caso base: $j=2$ $A[1]$ è banalmente ordinato ed è l'elemento $A[1]$ dell'array originale,
	\item Ipotesi induttiva: $A[1,j-1]$ è ordinato e contiene gli elementi $A[1,j-1]$ originali.
\end{enumerate}
\subsection{Complessita di Insertion-Sort}
\begin{lstlisting}[frame=single]
  insertion-sort(A,n){
    for(j = 2) to n do{ @:\hfill :@
      k = A[j]; @:\hfill Costo $C_1 * n-1$:@
      i = j - 1; @:\hfill Costo $C_2 * n-1$:@
      while(i > 0 && A[i] > k){ @:\hfill Costo $C_3 * \sum_{j=2}^{n}t_j$:@
        A[i + 1] = A[i]; @:\hfill Costo $C_4 * \sum_{j=2}^{n}(t_j-1)$:@
        i--; @:\hfill Costo $C_5 * \sum_{j=2}^{n}(t_j-1)$:@
      }
      A[i + 1] = k; @:\hfill Costo $C_6 * n-1$:@
    }
  }
\end{lstlisting}
\textbf{NB:} $t_j$ è il numero di volte che si valuta la guardia del \verb-while- nella $j_{esima}$ iterazione del \verb-for-
\subsubsection{Calcolo della complessità}
\begin{align*}
	T(n) = & \ C_1(n-1) + C_2(n-1) + C_3\sum_{j=2}^{n}t_j+(C_4+C_5)\sum_{j=2}^{n}(t_j-1)+C_6(n-1) \\
	=      & \ (C_1+C_2+C_6)(n-1)+C_3\sum_{j=2}^{n}t_j+(C_4+C_5)\sum_{j=2}^{n}(t_j-1)
\end{align*}\\
\textbf{Calcolo al caso ottimo:}
\begin{align*}
	t_j=1 & \ \forall_{j=2...n}
\end{align*}
\begin{align*}
	\sum_{j=2}^{n}t_j=\sum_{j=2}^{n}1=n-1 \\
	\sum_{j=2}^{n}(t_j-1)=0
\end{align*}
\begin{align*}
	T(n) = (n-1)(C_1+C_2+C_3+C_6) = an+b
\end{align*}
Complessità \textbf{LINEARE}
\newpage
\textbf{Calcolo al caso pessimo:}
\begin{align*}
	t_j=j & \ \forall_{j=2...n}
\end{align*}
\begin{align*}
	\sum_{j=2}^{n}t_j=(\sum_{j=1}^{n}t_j)-t_1=(\sum_{j=1}^{n}t_j)-1= (\sum_{j=1}^{n}t_j)-t_1=\frac{n(n-1)}{2}-1 \\
	\sum_{j=2}^{n}(j-1)=\sum_{j=1}^{n-1}j=\frac{n(n-1)}{2}
\end{align*}
\begin{align*}
	T(n) = & (n-1)(C_1+C_2+C_6) + C_3(\frac{n^2-n-2}{2})+(C_4+C_5)(\frac{n^2-n}{2}) \\
	=      & an^2+bn+c
\end{align*}
Complessità \textbf{QUADRATICA}\\~\\
\textbf{Calcolo al caso medio:}\\
\begin{align*}
	t_j=j/2 & \ \forall_{j=2...n}
\end{align*}
\begin{align*}
	\sum_{j=2}^{n}t_j=     & (\sum_{j=2}^{n}j/2)=\frac{n(n-1)}{2}-1/2 \\
	\sum_{j=2}^{n}(j/2-1)= & \sum_{j=1}^{n-1}j/2=\frac{n(n-1)}{4}
\end{align*}
\begin{align*}
	\text{Analogo al precedente}
\end{align*}
Complessità \textbf{QUADRATICA}
\newpage
%---------------------------------------------
\section{Selection-Sort}
\subsection{Problema}
\textbf{Input:} Array $A$ di $n$ numeri interi.\\
\textbf{Output:} Array $A$ ordinato: $A[1] \leq A[2] \leq A[3] \leq ... \leq A[n]$
\subsection{Codice}
\begin{lstlisting}[frame=single]
  selection-sort(A,n){
    for (i=1) to n-1 do { 
      minimo = i;
      for (j=i+1) to n do { 
        if(A[j]<A[minimo]) then 
          minimo = j; 
      }
      swap(A[i],A[minimo]); 
    }
  }
\end{lstlisting}
\subsection{Esempio}
\textbf{Input:} Array $A$ = \begin{tabular}{|c|c|c|c|c|c|}
	\hline
	5 & 2 & 4 & 6 & 1 & 3 \\
	\hline
\end{tabular}\\
\textbf{\\Ordinamento in azione:}\\~\\
$A=$\begin{tabular}{|c|c|c|c|c|c|}
	\hline
	\cellcolor{ferrarired}5 & 2 & 4 & 6 & \cellcolor{amber}1 & 3 \\
	\hline
\end{tabular}\hfill
Leggenda \begin{tabular}{|c|c|c|}
	\hline
	\cellcolor{brightgreen}Ordinato & \cellcolor{ferrarired}Da ordinare & \cellcolor{amber}Minimo \\
	\hline
\end{tabular}
$A=$\begin{tabular}{|c|c|c|c|c|c|}
	\hline
	\cellcolor{brightgreen}1 & \cellcolor{ferrarired}2 & 4 & 6 & 5 & 3 \\
	\hline
\end{tabular}\hfill$i=2$\\
$A=$\begin{tabular}{|c|c|c|c|c|c|}
	\hline
	\cellcolor{brightgreen}1 & \cellcolor{brightgreen}2 & \cellcolor{ferrarired}4 & 6 & 5 & \cellcolor{amber}3 \\
	\hline
\end{tabular}\hfill $i = 3$\\
$A=$\begin{tabular}{|c|c|c|c|c|c|}
	\hline
	\cellcolor{brightgreen}1 & \cellcolor{brightgreen}2 & \cellcolor{brightgreen}3 & \cellcolor{ferrarired}6 & 5 & \cellcolor{amber}4 \\
	\hline
\end{tabular}\hfill $i = 4$\\
$A=$\begin{tabular}{|c|c|c|c|c|c|}
	\hline
	\cellcolor{brightgreen}1 & \cellcolor{brightgreen}2 & \cellcolor{brightgreen}3 & \cellcolor{brightgreen}4 & \cellcolor{ferrarired}5 & 6 \\
	\hline
\end{tabular}\hfill $i = 5$\\
$A=$\begin{tabular}{|c|c|c|c|c|c|}
	\hline
	\cellcolor{brightgreen}1 & \cellcolor{brightgreen}2 & \cellcolor{brightgreen}3 & \cellcolor{brightgreen}4 & \cellcolor{brightgreen}5 & \cellcolor{brightgreen}6 \\
	\hline
\end{tabular}\hfill $i = 6 = n$\\
\subsection{Come funziona}
Il sottoarray $A[1,i-1]$ è, di fatto, ordinato. Per ogni elemento successivo al sottoarray ordinato lo inverto con l'elemento più piccolo che trovo nel restante array. Non necessito di controllare l'ultimo elemento perchè essendo l'ultimo rimasto è sicuramente l'elemento più grande dell'array.
\subsection{Invariante di ciclo}
All'inizio di ogni interazione $i_{esima}$ del \verb-for- esterno il sottoarray $A[1,i-1]$ è ordinato e contiene gli elementi dell array $A$ iniziale.
\newpage
\subsection{Complessita di Selection-Sort}
\begin{lstlisting}[frame=single]
  selection-sort(A,n){
    for (i=1) to n-1 do { @:\hfill n-1\ volte:@
      minimo = i;@:\hfill Cost:@
      for (j=i+1) to n do { @:\hfill n-i:@
        if(A[j]<A[minimo]) then @:\hfill Cost:@
          minimo = j; @:\hfill Cost:@
      }
      swap(A[i],A[minimo]); @:\hfill Cost:@
    }
  }
\end{lstlisting}
%\textbf{NB:} $t_j$ è il numero di volte che si valuta la guardia del \verb-while- nella $j_{esima}$ iterazione del \verb-for-
\subsubsection{Calcolo della complessità}
\begin{align*}
	C(n) = & \ \sum_{i=1}^{n-1}(\sum_{j=i+1}^{n}1) \\
	=      & \ \sum_{j=i+1}^{n}1 = n-1             \\
	=      & \ \frac{n(n-1)}{2}
\end{align*}
Complessità \textbf{QUADRATICA}. Non esistono: caso ottimo, caso medio e caso pessimo perchè i cicli devono comunque concludersi nella loro totalità.
\newpage
%---------------------------------------------
\section{Selection-Sort}
\subsection{Problema}
\textbf{Input:} Array $A$ di $n$ numeri interi.\\
\textbf{Output:} Array $A$ ordinato: $A[1] \leq A[2] \leq A[3] \leq ... \leq A[n]$
\subsection{Codice}
\begin{lstlisting}[frame=single]
  selection-sort(A,n){
    for (i=1) to n-1 do { 
      minimo = i;
      for (j=i+1) to n do { 
        if(A[j]<A[minimo]) then 
          minimo = j; 
      }
      swap(A[i],A[minimo]); 
    }
  }
\end{lstlisting}
\subsection{Esempio}
\textbf{Input:} Array $A$ = \begin{tabular}{|c|c|c|c|c|c|}
	\hline
	5 & 2 & 4 & 6 & 1 & 3 \\
	\hline
\end{tabular}\\
\textbf{\\Ordinamento in azione:}\\~\\
$A=$\begin{tabular}{|c|c|c|c|c|c|}
	\hline
	\cellcolor{ferrarired}5 & 2 & 4 & 6 & \cellcolor{amber}1 & 3 \\
	\hline
\end{tabular}\hfill
Leggenda \begin{tabular}{|c|c|c|}
	\hline
	\cellcolor{brightgreen}Ordinato & \cellcolor{ferrarired}Da ordinare & \cellcolor{amber}Minimo \\
	\hline
\end{tabular}
$A=$\begin{tabular}{|c|c|c|c|c|c|}
	\hline
	\cellcolor{brightgreen}1 & \cellcolor{ferrarired}2 & 4 & 6 & 5 & 3 \\
	\hline
\end{tabular}\hfill$i=2$\\
$A=$\begin{tabular}{|c|c|c|c|c|c|}
	\hline
	\cellcolor{brightgreen}1 & \cellcolor{brightgreen}2 & \cellcolor{ferrarired}4 & 6 & 5 & \cellcolor{amber}3 \\
	\hline
\end{tabular}\hfill $i = 3$\\
$A=$\begin{tabular}{|c|c|c|c|c|c|}
	\hline
	\cellcolor{brightgreen}1 & \cellcolor{brightgreen}2 & \cellcolor{brightgreen}3 & \cellcolor{ferrarired}6 & 5 & \cellcolor{amber}4 \\
	\hline
\end{tabular}\hfill $i = 4$\\
$A=$\begin{tabular}{|c|c|c|c|c|c|}
	\hline
	\cellcolor{brightgreen}1 & \cellcolor{brightgreen}2 & \cellcolor{brightgreen}3 & \cellcolor{brightgreen}4 & \cellcolor{ferrarired}5 & 6 \\
	\hline
\end{tabular}\hfill $i = 5$\\
$A=$\begin{tabular}{|c|c|c|c|c|c|}
	\hline
	\cellcolor{brightgreen}1 & \cellcolor{brightgreen}2 & \cellcolor{brightgreen}3 & \cellcolor{brightgreen}4 & \cellcolor{brightgreen}5 & \cellcolor{brightgreen}6 \\
	\hline
\end{tabular}\hfill $i = 6 = n$\\
\subsection{Come funziona}
Il sottoarray $A[1,i-1]$ è, di fatto, ordinato. Per ogni elemento successivo al sottoarray ordinato lo inverto con l'elemento più piccolo che trovo nel restante array. Non necessito di controllare l'ultimo elemento perchè essendo l'ultimo rimasto è sicuramente l'elemento più grande dell'array.
\subsection{Invariante di ciclo}
All'inizio di ogni interazione $i_{esima}$ del \verb-for- esterno il sottoarray $A[1,i-1]$ è ordinato e contiene gli elementi dell array $A$ iniziale.
\newpage
\subsection{Complessita di Selection-Sort}
\begin{lstlisting}[frame=single]
  selection-sort(A,n){
    for (i=1) to n-1 do { @:\hfill n-1\ volte:@
      minimo = i;@:\hfill Cost:@
      for (j=i+1) to n do { @:\hfill n-i:@
        if(A[j]<A[minimo]) then @:\hfill Cost:@
          minimo = j; @:\hfill Cost:@
      }
      swap(A[i],A[minimo]); @:\hfill Cost:@
    }
  }
\end{lstlisting}
%\textbf{NB:} $t_j$ è il numero di volte che si valuta la guardia del \verb-while- nella $j_{esima}$ iterazione del \verb-for-
\subsubsection{Calcolo della complessità}
\begin{align*}
	C(n) = & \ \sum_{i=1}^{n-1}(\sum_{j=i+1}^{n}1) \\
	=      & \ \sum_{j=i+1}^{n}1 = n-1             \\
	=      & \ \frac{n(n-1)}{2}
\end{align*}
Complessità \textbf{QUADRATICA}. Non esistono: caso ottimo, caso medio e caso pessimo perchè i cicli devono comunque concludersi nella loro totalità.
\newpage
%---------------------------------------------

\includepdf[page={1-10}]{tcscheat}
\end{document}